package evaluator.controller;

import java.util.LinkedList;
import java.util.List;

import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.model.Test;
import evaluator.repository.IntrebariRepository;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.repository.TesteRepository;
import evaluator.util.InputValidation;

public class AppController {
	
	private IntrebariRepository intrebariRepository;
    private TesteRepository testeRepository;
	
	public AppController() {
		intrebariRepository = new IntrebariRepository();
        testeRepository = new TesteRepository();
	}

    public Intrebare addNewIntrebare(Intrebare intrebare) throws DuplicateIntrebareException, InputValidationFailedException{

        InputValidation.validateIntrebare(intrebare);
        intrebariRepository.addIntrebare(intrebare);

        return intrebare;
    }

    public Test addNewTest(Test test) {
        testeRepository.adaugaTest(test);
        return test;
    }

    public void loadIntrebariFromFile(String f){
        intrebariRepository.setIntrebari(intrebariRepository.loadIntrebariFromFile(f));
    }

    public List<Intrebare> getIntrebari() {
        return intrebariRepository.getIntrebari();
    }

    public Test createNewTest() throws NotAbleToCreateTestException{

        if(intrebariRepository.getIntrebari().size() < 5)
            throw new NotAbleToCreateTestException("Nu exista suficiente intrebari pentru crearea unui test!(5)");
        if(intrebariRepository.getNumberOfDistinctDomains() < 5)
            throw new NotAbleToCreateTestException("Nu exista suficiente domenii pentru crearea unui test!(5)");

        List<Intrebare> testIntrebari = new LinkedList<Intrebare>();
        List<String> domenii = new LinkedList<String>();
        Intrebare intrebare;
        Test test = new Test();

        while(testIntrebari.size() != 5){
            intrebare = intrebariRepository.pickRandomIntrebare();

            if(!testIntrebari.contains(intrebare) && !domenii.contains(intrebare.getDomeniu())){
                testIntrebari.add(intrebare);
                domenii.add(intrebare.getDomeniu());
            }
        }
        test.setIntrebari(testIntrebari);
        addNewTest(test);
        return test;
    }

    public Statistica getStatistica() throws NotAbleToCreateStatisticsException{

        if(intrebariRepository.getIntrebari().isEmpty())
            throw new NotAbleToCreateStatisticsException("Repository-ul nu contine nicio intrebare!");

        Statistica statistica = new Statistica();
        for(String domeniu : intrebariRepository.getDistinctDomains()){
            // NU SE ADAUGA CE TREBUIE LA VALOAREA DIN HASHMAP
            statistica.add(domeniu, intrebariRepository.getNumberOfIntrebariByDomain(domeniu));
        }
        return statistica;
    }
}
