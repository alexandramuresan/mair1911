package evaluator.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;

import evaluator.controller.AppController;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.model.Test;

//functionalitati
//i.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//ii.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//iii.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {

	private static final String file = "D:\\Facultate\\An 3\\VVSS\\05-ProiectEvaluatorExamen\\05-ProiectEvaluatorExamen\\ProiectEvaluatorExamen\\src\\main\\java\\intrebari.txt";
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		
		AppController appController = new AppController();
        appController.loadIntrebariFromFile(file);
		
		boolean activ = true;
		String optiune = null;
		
		while(activ){
			
			System.out.println("");
			System.out.println("1.Lista intrebari.");
			System.out.println("2.Adauga intrebare noua.");
            System.out.println("3.Creeaza un test nou.");
            System.out.println("4.Statistica.");
            System.out.println("Introduceti optiunea: ");
			
			optiune = console.readLine();
			
			switch(optiune){
			case "1" :
                List<Intrebare> intrebari = appController.getIntrebari();
                for(Intrebare intrebare: intrebari) {
                    System.out.println(intrebare);
                }
				break;
			case "2" :
                System.out.println("Adauga enunt intrebare:");
                String enunt = console.readLine();
                System.out.println("Adauga varianta 1: ");
                String varianta1 = console.readLine();
                System.out.println("Adauga varianta 2: ");
                String varianta2 = console.readLine();
                System.out.println("Adauga varianta 3: ");
                String varianta3 = console.readLine();
                System.out.println("Adauga varianta corecta: ");
                String variantaCorecta = console.readLine();
                System.out.println("Adauga domeniu: ");
                String domeniu = console.readLine();
                Intrebare intrebare = new Intrebare(enunt,varianta1,varianta2,varianta3,variantaCorecta,domeniu);
                try {
                    appController.addNewIntrebare(intrebare);
                } catch(DuplicateIntrebareException | InputValidationFailedException ex ) {
                    System.out.println("Aceasta intrebare exista deja!");
                }
                break;
                case "3":
                    Test test = null;
                    try {
                        test = appController.createNewTest();
                    } catch(NotAbleToCreateTestException ex) {
                        System.out.println("Nu s-a putut crea acest test. Verifica numarul de intrebari/domenii disponibile.");
                    }
                    System.out.println(test);
                    break;
                case "4":
                    Statistica statistica = null;
                    try {
                        statistica = appController.getStatistica();
                    } catch(NotAbleToCreateStatisticsException ex) {
                        System.out.println("Nu exista intrebari din care sa se creeze o statistica.");
                    }
                    System.out.println(statistica);
                default:
				break;
			}
		}
		
	}

}
