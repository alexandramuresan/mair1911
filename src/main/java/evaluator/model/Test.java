package evaluator.model;

import java.util.LinkedList;
import java.util.List;

public class Test  {

	private List<Intrebare> intrebari;
	
	public Test() {
		intrebari = new LinkedList<Intrebare>(); 
	}
	
	public List<Intrebare> getIntrebari() {
		return intrebari;
	}
	
	public void setIntrebari(List<Intrebare> intrebari) {
		this.intrebari = intrebari;
	}

	@Override
    public String toString() {
	    String result = "";
	    for(Intrebare intrebare: this.intrebari) {
            result += intrebare + "\n";
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Intrebare) {
            Test i = (Test) obj;
            if (this.intrebari.containsAll(i.getIntrebari())) {
                return true;
            }
        }
        return false;
    }
}
