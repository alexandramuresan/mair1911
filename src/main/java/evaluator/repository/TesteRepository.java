package evaluator.repository;

import evaluator.model.Intrebare;
import evaluator.model.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexandra Muresan on 08-Mar-18.
 */
public class TesteRepository {

    private List<Test> teste;

    public TesteRepository() {
        teste = new ArrayList<>();
    }

    public boolean exists(Test t){
        for(Test test : teste)
            if(test.equals(t))
                return true;
        return false;
    }

    public void adaugaTest(Test t) {
        if(exists(t)) {
            return;
        }
        teste.add(t);
    }

    public List<Test> getTeste() {
        return teste;
    }
}
