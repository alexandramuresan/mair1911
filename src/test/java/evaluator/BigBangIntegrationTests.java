package evaluator;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by Alexandra Muresan on 25-Apr-18.
 */
public class BigBangIntegrationTests {

    private AppController controller;

    @Before
    public void initialize() {
        controller = new AppController();
    }

    @Test
    public void testA() {
        controller.getIntrebari().clear();
        Intrebare i = new Intrebare("Cat este 2 + 2?","1)2","2)3","3)4","3","Matematica");
        try {
            controller.addNewIntrebare(i);
        } catch(InputValidationFailedException | DuplicateIntrebareException ex) {
            assert false;
        }
        Intrebare i1 = new Intrebare("Cate anotimpuri sunt.","1)4","2)10","3)7","1","Cunostiinte generale");
        try {
            controller.addNewIntrebare(i1);
            assert false;
        }  catch(InputValidationFailedException | DuplicateIntrebareException ex) {
            assert true;
        }
    }

    @Test
    public void testB() {
        controller.getIntrebari().clear();
        Intrebare i1 = new Intrebare("abc?","1)a","2)b","3)c","1","abc");
        Intrebare i2 = new Intrebare("abcd?","1)a","2)b","3)c","2","abcd");
        Intrebare i3 = new Intrebare("abcde?","1)a","2)b","3)c","2","abcde");
        Intrebare i4 = new Intrebare("abcdef?","1)a","2)b","3)c","2","abcdef");
        Intrebare i5 = new Intrebare("abcdefg?","1)a","2)b","3)c","2","abcdefg");
        try {
            controller.addNewIntrebare(i1);
            controller.addNewIntrebare(i2);
            controller.addNewIntrebare(i3);
            controller.addNewIntrebare(i4);
            controller.addNewIntrebare(i5);
        } catch(DuplicateIntrebareException | InputValidationFailedException ex) {
            assert  false;
        }

        try {
            controller.createNewTest();
        } catch(NotAbleToCreateTestException ex) {
            assert false;
        }
    }

    @Test
    public void testC() {
        controller.getIntrebari().clear();
        Intrebare i1 = new Intrebare("Cat este 1+1?","1)2","2)3","3)4","1","Matematica");
        Intrebare i2 = new Intrebare("Cat este 2+2?","1)2","2)3","3)4","1","Matematica");
        Intrebare i3 = new Intrebare("Cate anotimpuri sunt?","1)2","2)3","3)4","1","Geografie");
        try {
            controller.addNewIntrebare(i1);
            controller.addNewIntrebare(i2);
            controller.addNewIntrebare(i3);
        } catch(DuplicateIntrebareException | InputValidationFailedException ex) {
            System.out.println(ex.getMessage());
        }

        try {
            Statistica statistica = controller.getStatistica();
            assertTrue(statistica.getIntrebariDomenii().size() == 2);

        } catch(NotAbleToCreateStatisticsException ex) {
            System.out.println(ex.getMessage());
            assert false;
        }
    }

    @Test
    public void testIntegration() {
        controller.getIntrebari().clear();
        Intrebare i1 = new Intrebare("abc?","1)a","2)b","3)c","1","abc");
        Intrebare i2 = new Intrebare("abcd?","1)a","2)b","3)c","2","abcd");
        Intrebare i3 = new Intrebare("abcde?","1)a","2)b","3)c","2","abcde");
        Intrebare i4 = new Intrebare("abcdef?","1)a","2)b","3)c","2","abcdef");
        Intrebare i5 = new Intrebare("abcdefg?","1)a","2)b","3)c","2","abcdefg");
        try {
            controller.addNewIntrebare(i1);
            controller.addNewIntrebare(i2);
            controller.addNewIntrebare(i3);
            controller.addNewIntrebare(i4);
            controller.addNewIntrebare(i5);
            assertTrue(controller.getIntrebari().size() == 5);
            evaluator.model.Test test = controller.createNewTest();
            assertTrue(test.getIntrebari().size() == 5);
            Statistica statistics = controller.getStatistica();
            assertTrue(statistics.getIntrebariDomenii().size() == 5);
        } catch(DuplicateIntrebareException | InputValidationFailedException | NotAbleToCreateTestException | NotAbleToCreateStatisticsException ex) {
            System.out.println(ex.getMessage());
            assert false;
        }
    }
}
