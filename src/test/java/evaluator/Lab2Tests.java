package evaluator;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Alexandra Muresan on 28-Mar-18.
 */
public class Lab2Tests {

    private AppController controller;

    @Before
    public void initialize() {
        controller = new AppController();
    }

    @Test
    public void tc1_ecp() {
        Intrebare i = new Intrebare("Cat este 2 + 2?","1)2","2)3","3)4","3","Matematica");
        try {
            controller.addNewIntrebare(i);
        } catch(InputValidationFailedException | DuplicateIntrebareException ex) {
            assert false;
        }
    }

    @Test
    public void tc2_ecp() {
        Intrebare i = new Intrebare("Cate anotimpuri sunt.","1)4","2)10","3)7","1","Cunostiinte generale");
        try {
            controller.addNewIntrebare(i);
            assert false;
        }  catch(InputValidationFailedException | DuplicateIntrebareException ex) {
            assert true;
        }
    }

    @Test
    public void tc6_ecp() {
        Intrebare i = new Intrebare("Cate ore studiezi?","1)12","1","3)2","1","Personal");
        try {
            controller.addNewIntrebare(i);
            assert false;
        }  catch(InputValidationFailedException | DuplicateIntrebareException ex) {
            assert true;
        }
    }

    @Test
    public void tc8_ecp() {
        Intrebare i = new Intrebare("Care este prima luna?","1)Ianuarie","2)Februarie","Martie","1","Cunostiinte generale");
        try {
            controller.addNewIntrebare(i);
            assert false;
        } catch(InputValidationFailedException | DuplicateIntrebareException ex) {
            assert true;
        }
    }

    @Test
    public void tc10_ecp() {
        Intrebare i = new Intrebare("Cat este radical din 4?","1)0","2)2","3)1","abc","Matematica");
        try {
            controller.addNewIntrebare(i);
            assert false;
        } catch(InputValidationFailedException | DuplicateIntrebareException ex) {
            assert true;
        }
    }

    @Test
    public void tc1_bva() {
        Intrebare i = new Intrebare("c","1)0","2)2","3)1","abc","Matematica");
        try {
            controller.addNewIntrebare(i);
            assert false;
        } catch(InputValidationFailedException | DuplicateIntrebareException ex) {
            assert true;
        }
    }

    @Test
    public void tc2_bva() {
        Intrebare i = new Intrebare("ce este abc ?","1)a","2)b","3)c","2","abc");
        try {
            controller.addNewIntrebare(i);
        } catch(InputValidationFailedException | DuplicateIntrebareException ex) {
            System.out.println(ex.getMessage());
            assert false;
        }
    }

    @Test
    public void tc6_bva() {
        Intrebare i = new Intrebare("ce este..?","1)a","2)","3)c","1","abc");
        try {
            controller.addNewIntrebare(i);
            assert false;
        } catch(InputValidationFailedException | DuplicateIntrebareException ex) {
            assert true;
        }
    }

    @Test
    public void tc10_bva() {
        Intrebare i = new Intrebare("ce este..?","1)a","2)a","3)c","1","");
        try {
            controller.addNewIntrebare(i);
            assert false;
        } catch(InputValidationFailedException | DuplicateIntrebareException ex) {
            assert true;
        }
    }

    @Test
    public void tc11_bva() {
        Intrebare i = new Intrebare("ce este..?","1)a","2)a","3)c","","abc");
        try {
            controller.addNewIntrebare(i);
            assert false;
        } catch(InputValidationFailedException | DuplicateIntrebareException ex) {
            assert true;
        }
    }

    @org.junit.Test
    public void test1() {
        controller.getIntrebari().clear();
        Intrebare i1 = new Intrebare("abc?","1)a","2)b","3)c","1","abc");
        Intrebare i2 = new Intrebare("abcd?","1)a","2)b","3)c","2","abcd");
        try {
            controller.addNewIntrebare(i1);
            controller.addNewIntrebare(i2);
        } catch(DuplicateIntrebareException | InputValidationFailedException ex) {
            assert  false;
        }

        try {
            controller.createNewTest();
            assert false;
        } catch(NotAbleToCreateTestException ex) {
            assert true;
        }
    }

    @org.junit.Test
    public void test2() {
        controller.getIntrebari().clear();
        Intrebare i1 = new Intrebare("abc?", "1)a", "2)b", "3)c", "1", "abc");
        Intrebare i2 = new Intrebare("abcd?", "1)a", "2)b", "3)c", "2", "abcd");
        Intrebare i3 = new Intrebare("abcde?", "1)a", "2)b", "3)c", "2", "abcde");
        Intrebare i4 = new Intrebare("abcdef?", "1)a", "2)b", "3)c", "2", "abcdef");
        Intrebare i5 = new Intrebare("abcdefg?", "1)a", "2)b", "3)c", "2", "abcdefg");
        try {
            controller.addNewIntrebare(i1);
            controller.addNewIntrebare(i2);
            controller.addNewIntrebare(i3);
            controller.addNewIntrebare(i4);
            controller.addNewIntrebare(i5);
        } catch (DuplicateIntrebareException | InputValidationFailedException ex) {
            assert false;
        }
    }
}
