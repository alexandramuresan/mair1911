package evaluator;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Test;
import evaluator.repository.IntrebariRepository;

import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;

/**
 * Created by Alexandra Muresan on 31-Mar-18.
 */
public class Lab3Tests {

    private AppController controller = new AppController();

    private void validateTest(Test test) {
        assertTrue(test.getIntrebari().size() == 5);
        assertTrue(test.getIntrebari().stream().distinct().collect(Collectors.toList()).size() == 5);
    }

    @org.junit.Test
    public void test1() {
        controller.getIntrebari().clear();
        Intrebare i1 = new Intrebare("abc?","1)a","2)b","3)c","1","abc");
        Intrebare i2 = new Intrebare("abcd?","1)a","2)b","3)c","2","abcd");
        try {
            controller.addNewIntrebare(i1);
            controller.addNewIntrebare(i2);
        } catch(DuplicateIntrebareException | InputValidationFailedException ex) {
            assert  false;
        }

        try {
            controller.createNewTest();
            assert false;
        } catch(NotAbleToCreateTestException ex) {
            assert true;
        }
    }

    @org.junit.Test
    public void test2() {
        controller.getIntrebari().clear();
        Intrebare i1 = new Intrebare("abc?","1)a","2)b","3)c","1","abc");
        Intrebare i2 = new Intrebare("abcd?","1)a","2)b","3)c","2","abcd");
        Intrebare i3 = new Intrebare("abcde?","1)a","2)b","3)c","2","abcde");
        Intrebare i4 = new Intrebare("abcdef?","1)a","2)b","3)c","2","abcdef");
        Intrebare i5 = new Intrebare("abcdefg?","1)a","2)b","3)c","2","abcdefg");
        try {
            controller.addNewIntrebare(i1);
            controller.addNewIntrebare(i2);
            controller.addNewIntrebare(i3);
            controller.addNewIntrebare(i4);
            controller.addNewIntrebare(i5);
        } catch(DuplicateIntrebareException | InputValidationFailedException ex) {
            assert  false;
        }

        try {
            controller.createNewTest();
        } catch(NotAbleToCreateTestException ex) {
            assert false;
        }
    }

    @org.junit.Test
    public void test3() {
        controller.getIntrebari().clear();
        Intrebare i1 = new Intrebare("abc?","1)a","2)b","3)c","1","abc");
        Intrebare i2 = new Intrebare("abcd?","1)a","2)b","3)c","2","abcd");
        Intrebare i3 = new Intrebare("abcde?","1)a","2)b","3)c","2","abcd");
        Intrebare i4 = new Intrebare("abcdef?","1)a","2)b","3)c","2","abcd");
        Intrebare i5 = new Intrebare("abcdefg?","1)a","2)b","3)c","2","abcdefg");
        try {
            controller.addNewIntrebare(i1);
            controller.addNewIntrebare(i2);
            controller.addNewIntrebare(i3);
            controller.addNewIntrebare(i4);
            controller.addNewIntrebare(i5);
        } catch(DuplicateIntrebareException | InputValidationFailedException ex) {
            assert  false;
        }

        try {
            controller.createNewTest();
            assert false;
        } catch(NotAbleToCreateTestException ex) {
            assert true;
        }
    }
}
