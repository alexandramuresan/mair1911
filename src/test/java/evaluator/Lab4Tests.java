package evaluator;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

/**
 * Created by Alexandra Muresan on 25-Apr-18.
 */
public class Lab4Tests {

    private AppController controller;

    @Before
    public void initialize() {
        controller = new AppController();
    }

    @Test
    public void testNonValid() {
        controller.getIntrebari().clear();
        try {
            controller.getStatistica();
            assert false;
        } catch (NotAbleToCreateStatisticsException ex) {
            System.out.println(ex.getMessage());
            assert true;
        }
    }

    @Test
    public void testValid() {
        controller.getIntrebari().clear();
        Intrebare i1 = new Intrebare("Cat este 1+1?","1)2","2)3","3)4","1","Matematica");
        Intrebare i2 = new Intrebare("Cat este 2+2?","1)2","2)3","3)4","1","Matematica");
        Intrebare i3 = new Intrebare("Cate anotimpuri sunt?","1)2","2)3","3)4","1","Geografie");
        try {
            controller.addNewIntrebare(i1);
            controller.addNewIntrebare(i2);
            controller.addNewIntrebare(i3);
        } catch(DuplicateIntrebareException | InputValidationFailedException ex) {
            System.out.println(ex.getMessage());
        }

        try {
           Statistica statistica = controller.getStatistica();
            assertTrue(statistica.getIntrebariDomenii().size() == 2);

        } catch(NotAbleToCreateStatisticsException ex) {
            System.out.println(ex.getMessage());
            assert false;
        }

    }


}
