package evaluator;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

/**
 * Created by Alexandra Muresan on 25-Apr-18.
 */
public class TopDownIntegrationTests {

    private AppController controller;

    @Before
    public void initialize() {
        controller = new AppController();
    }

    @Test
    public void testA() {
        controller.getIntrebari().clear();
        Intrebare i = new Intrebare("Cat este 2 + 2?","1)2","2)3","3)4","3","Matematica");
        try {
            controller.addNewIntrebare(i);
        } catch(InputValidationFailedException | DuplicateIntrebareException ex) {
            assert false;
        }
        Intrebare i1 = new Intrebare("Cate anotimpuri sunt.","1)4","2)10","3)7","1","Cunostiinte generale");
        try {
            controller.addNewIntrebare(i1);
            assert false;
        }  catch(InputValidationFailedException | DuplicateIntrebareException ex) {
            assert true;
        }
    }

    @Test
    public void integration1() {
        controller.getIntrebari().clear();
        Intrebare i = new Intrebare("Cat este 2 + 2?","1)2","2)3","3)4","3","Matematica");
        Intrebare i2 = new Intrebare("abcd?","1)a","2)b","3)c","2","abcd");
        Intrebare i3 = new Intrebare("abcde?","1)a","2)b","3)c","2","abcde");
        Intrebare i4 = new Intrebare("abcdef?","1)a","2)b","3)c","2","abcdef");
        Intrebare i5 = new Intrebare("abcdefg?","1)a","2)b","3)c","2","abcdefg");
        try {
            controller.addNewIntrebare(i);
            controller.addNewIntrebare(i2);
            controller.addNewIntrebare(i3);
            controller.addNewIntrebare(i4);
            controller.addNewIntrebare(i5);
            assertTrue(controller.getIntrebari().size() == 5);
            evaluator.model.Test test = controller.createNewTest();
            assertTrue(test.getIntrebari().size() == 5);
        } catch(InputValidationFailedException | DuplicateIntrebareException | NotAbleToCreateTestException ex) {
            assert false;
        }
    }

    @Test
    public void integration2() {
        integration1();
        try {
            Statistica statistica = controller.getStatistica();
            assertTrue(statistica.getIntrebariDomenii().size() == 5);
        } catch(NotAbleToCreateStatisticsException ex) {
            System.out.println(ex.getMessage());
            assert false;
        }
    }
}
